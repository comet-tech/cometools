---
title: Cometools Document

language_tabs: # must be one of https://git.io/vQNgJ
  - python: Code

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - intergration
  - establish_web_server
  - develop_web_api
  - work_on_async_tasks
  - authentication
  - using_oauth
  - logging
  - idaas

search: true
---

# Introduction

"cometools" is the share library for comet-tech

This doc will cover the usage for cometools library.