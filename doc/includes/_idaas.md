# IDaaS SSO

**Cometools** integrate [Aliyun IDaaS](https://help.aliyun.com/document_detail/112323.html?spm=a2c4g.11186623.6.542.56413030iUZUkA) to provide SSO for our applications. Just setup a small configuration, you can use IDaaS as a single sign on point for your application. After configuration, just type your `username` and `password` in IDaaS, you can sign in the application without creating a new account.

## Usage/Integration with `cometools.idaas.oauth` module

We assume you are familiar with [OAuth2.0](https://docs.authlib.org/en/latest/oauth/2/intro.html#intro-oauth2). If not, please have a look at it.

When integrating with IDaaS in your application, your application stands for an oauth client which need to grab user infomation(i.e. username) from IDaaS service, so your application must be registered as an oauth app in IDaaS.

```python
from cometools.idaas.oauth import OAuthClient
# Step 1
client = OAuthClient(
        "https://irlcaqkinh.login.aliyunidaas.com", # idaas service endpoint
        "CLIENT_ID", # client id
        "CLIENT_SECRET", # client secret
        "REDIRECT_URI", # redirect uri
    )
```
**Step 1**: As an oauth app, you should get your `client_id` and `client_secret`, pass them along with IDaaS endpoint to `OAuthClient` class.

**Step 2**: After creating the oauth client for IDaaS, you should get an authorize url by `client.get_authorize_url()`, you should setup your application to redirect user to this url and finish user authentication (i.e. type their IDaaS username and password), if authentication is successful, IDaaS will redirect user to a pre-defined `callback url` which you should setup in the registration setup for your application as an oauth app in IDaaS. And you should get an `authorization code`

```python
# Step 3
client.auth(code) # code is parsed from `callback url`
client.get_user()
```

**Step 3**: You can pass the `authorization code` to `client.auth` method and the oauth authentication should be finished, now you can use `client.get_user()` to get the user who is logged in through IDaaS, the next step depends on your application, usually we should create a user session for him/her.

## Usage/Integration with `cometools.idaas.jwt` module

```python
from cometools.idaas.jwt import IDTokenResolver
# Step 1: You must first register your app as an JWT app on IDaaS and get a public key, then setup up your resolver in your app
resolver = IDTokenResolver("JWT_APP_PUB_KEY")
# Step 2: You may signin from IDaaS, after your signin, 
# IDaaS will redirect you back to your app with an ID_TOKEN 
# which contains user information, then you should use your resolver to resolve it to get your login information
user = resolver.resolve(ID_TOKEN)
```

**Cometools** also provide JWT resolver to resolve `id_token` from IDaaS to get user information.
