# Intergration

## Pip install

``` shell
-e git+https://gitlab.com/comet-tech/cometools.git@0.1.0#egg=cometools 
```

First of all , you need install the package from pip by editing your `pip-requirement.txt` file.

Note that `0.1.0` is the version number (git branch) of the cometools. You need choose the right version for your project.

## Features

Cometools contains below features:

- Core Infrastructure
- Tools & Libs
- Authentication System
- OAuth Framework

We'll introduce each of them in details.

To get an example, check `iu_demo` project

## Code Structure

Cometools are structures with below components:

- api: Base Web API meta-class for falcon framework
- core: Base Infrastructure Framework. We use [falcon](https://falcon.readthedocs.io/en/stable/user/index.html) for web server and [celery](http://www.celeryproject.org/) for async tasks.
  - server.py: for web server (falcon)
  - tasks.py: for async tasks (celery)
  - worker.py: for async tasks executor (celery)
- hooks: Pre-defined [Falcon Hooks](https://falcon.readthedocs.io/en/stable/api/hooks.html) for general usage.
- middlewares: Pre-defined [Falcon Middleware](https://falcon.readthedocs.io/en/stable/api/middleware.html) for general usage
- mixin: Mixin classes for Mongo or Redis models
- utils: Tools or libs for general usage