# Logging

**Cometools** use python standard [logging]((https://docs.python.org/3/library/logging.config.html?highlight=logging%20dict)) module for logging, and the logger configuration can be written in the configuration file (e.g. `server.yml`).

## Coloredlogs

``` yaml
  coloredlogs:
    level: DEBUG
    milliseconds: true
```
**Cometools** also introduce [coloredlogs](https://coloredlogs.readthedocs.io/) module to print logs on console with colored style, enable and configure it by adding `coloredlogs` session in configuration file.

## Basic logging configuration

```python
from cometools.base import BaseLogConfigurator
BaseLogConfigurator.config_root()
```

You can use `cometools.log.base` for basic logging configuration.


## Logstash
We provide logstash configuration for logging system, what you need to provide is the logstash server hostname and port, so that when you logging something by with a logger prefixed by `logstash`, it will send logs to logstash server (also to console).

Example usage are:

```python
#Step 1
from cometools.log.logstash_conf import LogstashConfigurator
LogstashConfigurator.config_logstash_tcp(LOGSTASH_HOST,LOGSTASH_TCP_PORT)
#LogstashConfigurator.config_logstash_ucp(LOGSTASH_HOST,LOGSTASH_UDP_PORT)
```

**Step 1:** Use `LogstashConfigurator` to configure your logstash server. For now, you can configure your logstash server through tcp or udp, it depends on your server configuration.

```python
#Step 2
logger = logging.getLogger("logstash.database.table")
logger.info(MESSAGE, extra={"data_key":"data_value"})
```

**Step 2:** Then just write your logs as normal, and they will be directed to logstash service.

IMPORT: please note that only the logs with __INFO__ or higher level will be sent if you write logs with logger prefixed by `logstash`, DO NOT use `logger.debug` when you want to write logs to logstash.