# Establish Web Server

## Define Server Class

``` python
import cometools.core.server as s
import cometools.utils.password as p
import logging
logging.basicConfig(level=logging.DEBUG)
from docopt import docopt
from demo.core.url_mapping import API_ROUTER
import os
from cometools.core.tasks import get_all_tasks
from cometools.middlewares.validation import RequestValidationMiddleware, RequireJSONMiddleware
from cometools.middlewares.session import SessionMiddleware
import cometools.utils.unit_convertor as uc
import demo.tasks as t

class IUBackendService(s.BaseBackendService):
    proj_name = 'demo' # Give a unique name for your project

    def __init__(self, options):
        async_tasks = get_all_tasks(t) # Register tasks
        super(IUBackendService, self).__init__(
            options,
            API_ROUTER,
            middlewares = [
                RequireJSONMiddleware(),
                RequestValidationMiddleware(),
                SessionMiddleware(),
            ],
            async_tasks = async_tasks,
        )
```

To get start on your development, you need first define your web service class. Let's use the DEMO project as example.

The `options` would contain some customized arguments and `API_ROUTER` would be your URL mapping package.

In `middlewares`, you can add middlewares to handle each API request before hitting your handler.

## Add Main Entrance

``` python
CONFIG_FILE = '/etc/server.yml'

def main():
    options = ConfigParser.parse_config_file(CONFIG_FILE)
    # Disable rate limiter for web server
    options['rate-limiter']['enable'] = False
    application = IUBackendService(
        options,
    )
    gunicorn_app = s.GunicornApp(application, options['gunicorn'])
    application.logger.info('Server starting...')
    gunicorn_app.run()

if __name__ == "__main__":
    main()
```


Add main entry to start your server. We use [gunicore](https://gunicorn.org/) to speed up the server.

Server options are orgnized as YAML file and you need deploy it to your server machine. Here we put the file under `/etc/server.yml`

## Server Options

``` yaml
---
  env:
    proj: demo
    config: stage
  falcon:
    resp_options:
      secure_cookies_by_default: False
  session:
    login_expire: 30d
    guest_expire: 1d
    user_expire: 1m

...
```
``` python
class ConfigParser(s.ConfigParser):
    @classmethod
    def convert_mapping(cls):
        return {
            'max-memory-per-child': uc.memory_to_kb,
            'login_expire': uc.time_to_second,
            'guest_expire': uc.time_to_second,
            'user_expire': uc.time_to_second,
        }
```

Option file would looks like this.

`ConfigParser` class would define some convertor for option fields, to make your configure file more readable.

## Register Meta Data

``` python
from demo.model.mongo.oauth2_client import OAuth2Client
from demo.model.mongo.oauth2_token import OAuth2Token
from demo.model.mongo.oauth2_authorization_code import AuthorizationCode
from demo.model.mongo.rbac import Permission, Role
from demo.model.redis_keys.session import Session
from demo.model.mongo.user import User
from cometools.utils.oauth.grants import AuthorizationCodeGrant
from cometools.meta import Meta
Meta.config(
    oauth_client_model=OAuth2Client,
    oauth_token_model=OAuth2Token,
    oauth_authcode_model=AuthorizationCode,
    oauth_grants=[
        AuthorizationCodeGrant
    ],
    permission_model=Permission,
    auth_group_model=Role,
    user_model=User,
    session_redis_model=Session,
)
```

To get advantage of exists framework, you need import some of your classses and config it in `Meta` object.

Currently Meta object require below fields:
- oauth_client_model, oauth_token_model, oauth_authcode_model, oauth_grants: See OAuth Charactor
- permission_model, auth_group_model, user_model, session_redis_model: See Authentication Charactor