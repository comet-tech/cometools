# Authentication Framework

Cometools use a Role-Based-Access-Control system to manage permissions.

## User

```python
# Default fields
    username = f.StringField()
    password = f.StringField()
    permissions = f.ListField(f.EmbeddedDocumentField('Permission'),  default=[])
    roles = f.ListField(f.ObjectIdField(),  default=[])
```

The basic user of your system should be defined as a mongo model and including below fields by default:

- username
- password
- permissions
- roles

## Permission

The base unit of authentication system, it's an EmbeddedDocument and would be a combination of below 3 items:

- Resource: Usually to be an URI, could also use customized flag as well
- Action: HTTP methods like GET/POST/PUT/PATCH/DELETE
- Allow: Whether to allow the access

## AuthGroup

``` python
# Default fields:
    name = f.StringField()
    description = f.StringField()
    parents = f.ListField(f.ObjectIdField())
    # Parents auth_group to inherit, like 'can_place_order' should be one of the parent of 'logistic_user'
    permissions = f.ListField(f.EmbeddedDocumentField('Permission'))
    # Isolated permissions
    tags = f.ListField(f.StringField())
```

AuthGroup is a group of permissions which can be easily assigned to a user or contract another group.

For example: We can have a group named as `can_place_order` and put all related permission in it, and then define a `logistic_user` group which would include it and assign to a user.

`can_place_order` is working as an 'abstract group' in this case and `logistic_user` is a common user role, but both of them are validate AuthGroup item.

In a common use case, ENG should create 'abstract groups' by writing permission rules using URL, and business admin could help on creating and managing the 'role' and assign to correct users.

We provide a field called `tags` to help identify different group types.

## Permission check mechanism

```python
# In PermissionMixin model

    @classmethod
    def check_permissions(cls, resource, action, permissions, default_allow=False):
        for p in permissions:
            r = cls.santize_url_resource(resource, p)
            pattern = '^{url_pattern}'.format(
                url_pattern = p.resource
            )
            if re.match(pattern, r) and (
                    action in p.actions or p.actions == []):
                return p.allow
        else:
            return default_allow
```

We provide a function to check permissions in `PermissionMixin` model.

You need provide below inputs to the function:

- resource: The resource you would check (URI)
- action: The action you would check
- permissions: The permission list you have
- default_allow: Should we pass the check if the URI is not defined explicitly

<aside class="notice"> The permission in the list would be checked one by one and stop once hit a matched rule - which means the starting permissions would have the highest priority. </aside>

We will order the list by below rules:

1. For a user, permission defined directly in `permission` field have the highest priority. Then we'll check the auth_group in `roles` field one by one.
2. For an auth_group, permission defined directly would have a higher priority than the parents' permissions - which means we can override the parents rule by re-define it.


## Session

Session is used for recording the status of a user connection (like the login information), it is stored in a redis hash key and contains below attributes:

| Attribute        | Description                                                          |
| :--------------- | :------------------------------------------------------------------- |
| user_cache       | a pickle dump of login user                                          |
| user_expire      | the expire ts of the user cache (will re-cache the user when expire) |
| user_permissions | a pickle dump for login user's permissions                           |
| user             | A property to get the current login user.                            |
| user_id          | Current login user's id, could be a guest id if no login             |
| snapshot         | A memory snapshot to reduce redis call                               |


It provide below functions

| Function           | Description                                                                 |
| :----------------- | :-------------------------------------------------------------------------- |
| create_new_session | Create a new session. By default we'll assign a guest user to this session. |
| expire_user        | Expire the user cache to force a reload                                     |
| is_guest           | Whether it's a guest user                                                   |
| login              | login a user to current session                                             |
| logout             | logout current user                                                         |

Session would have an expired time, after expire the redis key would be deleted automatically

## Middleware

``` python
class SessionMiddleware(object):
    def process_resource(self, req, resp, resource, params):
        ''' Save app options.env to context
        '''
        req.context['env'] = resource.application.options['env']

    def process_request(self, req, resp):
        ''' Assign a server-side session for each access
        '''
        cookies = req.cookies
        session_id = cookies.get('session_id')
        if session_id is None:
            session_id = Session.create_new_session()
            resp.set_cookie('session_id', session_id)
        session = Session(session_id)
        if not session.is_valid():
            session_id = Session.create_new_session()
            resp.set_cookie('session_id', session_id)
            session = Session(session_id)
        req.context['session'] = session
```

We have define a `SessionMiddleware` to detect the session and send it to API handler.

## Hooks

```python
# Usage: wrap a decorator to your function

    @falcon.before(PermissionRequired())
    def on_get(self, req, resp):
        ...
```

Hooks help validate the api authentication.

| Hook Class         | Description                                                    |
| :----------------- | :------------------------------------------------------------- |
| LoginRequired      | This API require a login user                                  |
| PermissionRequired | This API will check login user's permission                    |
| OAuthRequired      | This API will check access token's permission (See oauth part) |