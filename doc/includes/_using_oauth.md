# Using OAUTH

## Prerequisite

- Your project has integrate [Cometools Auth System](#authentication-framework)
- Understand OAuth2 flow, reference [this doc](https://docs.authlib.org/en/latest/basic/oauth2.html).

## Definitions

Let's define some roles here:

| Role      | Description                           |
| :-------- | :------------------------------------ |
| Client    | 3rd party app, like ERP, Carrier e.g. |
| User      | Resource owner, like merchant.        |
| Server    | Your system like lsp/wms              |
| Developer | The developer of the system           |

## Limitation

Currently we only integrate below grants with `Beraer Token`:
- authorization_code
- refresh_token
- client_credentials

## Integrate OAuth Server

### Prepare DB models

```python
# Most functions has already defined in cometools
# so you just need define the database and index like below

from cometools.mixin.oauth *

class OAuth2Token(Document, MongoMixin, TokenMixin):
    ''' Model to store oauth token
    user = f.ObjectIdField()
    client_id = f.ObjectIdField()
    token_type = f.StringField(choices=ALLOWED_TOKEN_TYPE)
    access_token = f.StringField()
    refresh_token = f.StringField()
    scope = f.ListField(f.StringField())
    revoked = f.BooleanField(default=False)
    expires_in = f.IntField(default=0)
    '''
    meta = TokenMixin.meta
    meta['db_name'] = '<your-db>'

class OAuth2Client(Document, MongoMixin, ClientMixin):
    ''' Model to store oauth client
    ALLOWED_RESPONSE_TYPE = [
        'code',
        'token',
    ]
    ALLOWED_GRANT_TYPE = [
        'authorization_code',
        'refresh_token',
        'client_credentials',
    ]
    ALLOWED_CLIENT_AUTH_METHOD = [
        'client_secret_post'
    ]

    user = f.ObjectIdField()
    client_secret = f.StringField()
    client_name = f.StringField()
    redirect_uris = f.ListField(f.StringField())
    default_redirect_uri = f.StringField()
    scope = f.ListField(f.StringField())
    response_types = f.ListField(f.StringField(choices=ALLOWED_RESPONSE_TYPE))
    grant_type = f.ListField(f.StringField(choices=ALLOWED_GRANT_TYPE))
    token_endpoint_auth_method = f.StringField(choices=ALLOWED_CLIENT_AUTH_METHOD)
    '''
    meta = ClientMixin.meta
    meta['db_name'] = '<your-db>'

class AuthorizationCode(Document, MongoMixin, AuthorizationCodeMixin):
    ''' Model to store authorization code
    user = f.ObjectIdField()
    client_id = f.ObjectIdField()
    code = f.StringField()
    redirect_uri = f.StringField()
    response_type = f.StringField()
    scope = f.ListField(f.StringField())
    '''
    meta = AuthorizationCodeMixin.meta
    meta['db_name'] = '<your-db>'

# Config meta like below
from demo.model.mongo.oauth2_client import OAuth2Client
from demo.model.mongo.oauth2_token import OAuth2Token
from demo.model.mongo.oauth2_authorization_code import AuthorizationCode
from demo.model.mongo.rbac import Permission, Role
from demo.model.redis_keys.session import Session
from demo.model.mongo.user import User
from cometools.utils.oauth.grants import AuthorizationCodeGrant
from cometools.utils.oauth.grants import RefreshTokenGrant
from cometools.utils.oauth.grants import ClientCredentialsGrant
from cometools.meta import Meta
Meta.config(
    oauth_client_model=OAuth2Client,
    oauth_token_model=OAuth2Token,
    oauth_authcode_model=AuthorizationCode,
    oauth_grants=[
        AuthorizationCodeGrant,
        RefreshTokenGrant,
        ClientCredentialsGrant,
    ],
    permission_model=Permission,
    auth_group_model=AuthGroup,
    user_model=User,
    session_redis_model=Session,
    settings=settings,
)

```

OAuth server require below models:

1. Client - Store oauth client meta data
2. Token - Store access tokens
3. AuthorizationCode - Store authorization code to grant access

Cometools has provided some mixins for it, please import and inherit under `from cometools.mixin.oauth` path.

Developer also need add these models into `Meta` object. When inherit mixins, developer must give db name.

Also need config `AuthorizationCodeGrant`, `RefreshTokenGrant`, `ClientCredentialsGrant` when setup meta data.

### Register OAuth APIs

```python
import demo.api as a
import cometools.api.oauth as oa
API_ROUTER = [
    ...
    ('/api/oauth2/authorize', oa.OAuth2AuthorizationApi),
]

## GET:
# response_type
#     REQUIRED.  Value MUST be set to "code".

# client_id
#     REQUIRED.  The client identifier as described in Section 2.2.

# redirect_uri
#     OPTIONAL.  As described in Section 3.1.2.

# scope
#     OPTIONAL.  The scope of the access request

# state
#     RECOMMENDED.  An opaque value used by the client to maintain
#     state between the request and callback.  The authorization
#     server includes this value when redirecting the user-agent back
#     to the client.  The parameter SHOULD be used for preventing
#     cross-site request forgery as described in Section 10.12.

## POST: Same as GET

## POST(issueToken):
# grant_type
#         REQUIRED.  Value MUST be set as "authorization_code"/"refresh_token"....

# code
#         REQUIRED.  The authorization code received from the
#         authorization server.

# redirect_uri
#         REQUIRED, if the "redirect_uri" parameter was included in the
#         authorization request as described in Section 4.1.1, and their
#         values MUST be identical.

# client_id
#         REQUIRED, if the client is not authenticating with the
#         authorization server as described in Section 3.2.1.
```

OAuth server require an external API to process authorization process.

Cometools has integrate this API under `cometools.api.oauth`, named as `OAuth2AuthorizationApi`.

This API provide functions as below:

| action           | description                                         |
| :--------------- | :-------------------------------------------------- |
| GET              | Initail OAuth progress                              |
| POST             | User approve access                                 |
| POST(issueToken) | Verify authorization code and generate access token |

Register APIs in your `url_mapping` to serve oauth authentication requests.

### Protect your resource with OAuth

```python
class OAuthDemoApi(BaseApiResource):

    @falcon.before(OAuthRequired())
    def on_get(self, req, resp):
        token = req.context['token']
        user = token.get_user()
        self.response(resp, {
            'message': 'Hello %s' % user.username
        })
```

In your project, add hook to protect a resource api by oauth

Notes:

- Using req.context['token'] to get the token item, and token.get_user() to get user item if need
- Access is allowed only if
  - token scope include this resource
  - token user's permission can access this resource.
- You can still use req.context['user'] to get the login user's information.

For example: Supposing ERP has a user `erp_user`, it create an OAuth client `my_erp` and successfully get the access token from `user_a`, when erp use this token to access a resource:

- `token.get_user()` will return `user_a` (This token represent user_a's `resource`)
- req.context['user'] will return `erp_user` (Request session is under `erp_user`)
- `token.get_client_id()` will return `my_erp`'s id (This token belongs to client `my_erp`)

## Integrate OAuth Client

Auth lib provide a oauth client framework to easily access oauth system.

Please follow below steps to integrate it.

### Prepare DB Models

```python
class OAuth2Registry(Document, MongoMixin, ClientRegistryMixin):
    ''' Store OAuth Server meta data
    name = f.StringField()
    client_id = f.StringField()
    client_secret = f.StringField()
    access_token_url = f.StringField()
    authorize_url = f.StringField()
    api_base_url = f.StringField()
    client_kwargs = f.DictField()
    '''
    meta = ClientRegistryMixin.meta
    meta['db_name'] = 'your-db'

class OAuth2UserToken(Document, MongoMixin, UserTokenMixin):
    ''' Store access token granted by user
    user_id = f.ObjectIdField()
    name = f.StringField()
    token_type = f.StringField()
    access_token = f.StringField()
    refresh_token = f.StringField()
    expires_in = f.IntField()
    expires_at = f.IntField()
    scope = f.DictField()
    '''
    meta = UserTokenMixin.meta
    meta['db_name'] = 'your-db'

```

Developer need below model to config as OAuth Client:

| Model          | Description                                       |
| :------------- | :------------------------------------------------ |
| ClientRegistry | Store the meta data of the client you need access |
| UserToken      | Store the access token granted by user            |

### Developer UI and API

```python
# Register OAuthAuthorizationUrl
from cometools.api.oauth_client import OAuthAuthorizationUrl as _OAuthAuthorizationUrl
class OAuthAuthorizationUrl(_OAuthAuthorizationUrl):
    URI = '/api/oauth2_client/authorization_url'

# Register OAuthRedirectCallback
from cometools.api.oauth_client import OAuthRedirectCallback as _OAuthRedirectCallback
class OAuthRedirectCallback(_OAuthRedirectCallback):
    URI = '/api/oauth2_client/redirect_callback'
```

To connect OAuth server, developer usually need provider UI / API as below:

1. UI button to initial the request, like a button "login with XXX account"
2. When user click this button, sending a GET request to server's authorization API, and then server will return an redirect URL link. `cometools.api.oauth_client.OAuthAuthorizationUrl` API can help generate this GET request. Developer just need let UI page send an AJAX request to this API and provide client registry's name.
3. Once get API callback, perform HTTP redirect on webpage.
4. On server's web page, user need login his account and approve the request. Then server will send authorization code to client's callback URL
5. `cometools.api.ouath_client.OAuthRedirectCallback` API can accept this callback request and store token in UserToken model.

### Access OAuth protected resources

```python
# Get an oauth_session first
oauth_session = Meta.oauth_client.register('lsp')

# 1. By request
oauth_session.get('demo/oauth2_required', request=req)

# 2. By User
token = oauth_session.get_token_by_user(user)
oauth_session.get('demo/oauth2_required', token=token)

# 3. By Client Credentials
ret = oauth_session.get('demo/oauth2_required')
```

Developer has three ways to acquire oauth protected resource:

1. By HTTP request
2. By User
3. By Client Credential

Once token is expired, our client can query a new one and refresh the db records automatically

<aside class="notice">
When client need access its own resource, using client credential way to get a token grant by itself.
</aside>