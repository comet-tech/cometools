from cometools.api.base import BaseApiResource
from cometools.meta import Meta
from cometools.hooks.auth import LoginRequired
import falcon
import re

class OAuthAuthorizationUrl(BaseApiResource):
    URI = '/api/oauth2_client/authorization_url'

    @falcon.before(LoginRequired())
    def on_get(self, req, resp):
        oauth = Meta.oauth_client
        cfg = oauth.load_config(req.params.get('name'), {})
        oauth_session = oauth.register(req.params.get('name'))
        rv = oauth_session.create_authorization_url()
        # Add authorization information in session
        oauth_session.save_authorize_data(req, redirect_uri=cfg['client_kwargs'].get('redirect_uri'), **rv)
        self.response(resp, {
            'url': rv['url']
        })

class OAuthRedirectCallback(BaseApiResource):
    def handle_token(self, req, resp, token, cfg):
        ''' Can be override, default: save token '''
        session = req.context['session'] 
        token.update({
            'name': cfg['name'],
        })
        Meta.ouath_user_token_model.update({
            'name': token.pop('name'),
            'user_id': session.user.id,
        }, {
            '$set': token
        }, upsert=True)
        self.response(resp, {
            'message': 'Token saved'
        })

    def on_get(self, req, resp):
        session = req.context['session']
        oauth = Meta.oauth_client
        # Get app name by state
        for k, v in session.snapshot.items():
            regex = re.match('^_(.*)_authlib_state_$', k.decode('ascii'))
            if not regex:
                continue
            if v.decode('ascii') == req.params.get('state'):
                app_name = regex.group(1)
                break
        else:
            raise falcon.HTTPBadRequest(
                title='Bad request',
                description='Missing scope in request',
            )
        cfg = oauth.load_config(app_name, {})
        oauth_session = oauth.register(app_name)
        token = oauth_session.authorize_access_token(req)

        self.handle_token(req, resp, token, cfg)