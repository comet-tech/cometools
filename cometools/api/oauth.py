import jsonschema
import falcon
from cometools.meta import Meta
from cometools.hooks.auth import LoginRequired, PermissionRequired
from cometools.hooks.validation import JsonSchema
from cometools.api.base import BaseApiResource
from bson import ObjectId

class BaseOAuthApi:
    def oauth_response(self, resp, status_code, payload, headers):
        attr_name = 'HTTP_%s' % status_code
        resp.status = getattr(falcon, attr_name)
        resp.media = payload
        for k, v in headers:
            resp.append_header(k, v)

class OAuth2AuthorizationApi(BaseOAuthApi, BaseApiResource):
    @falcon.before(LoginRequired())
    def on_get(self, req, resp):
        '''
            response_type
                REQUIRED.  Value MUST be set to "code".

            client_id
                REQUIRED.  The client identifier as described in Section 2.2.

            redirect_uri
                OPTIONAL.  As described in Section 3.1.2.

            scope
                OPTIONAL.  The scope of the access request

            state
                RECOMMENDED.  An opaque value used by the client to maintain
                state between the request and callback.  The authorization
                server includes this value when redirecting the user-agent back
                to the client.  The parameter SHOULD be used for preventing
                cross-site request forgery as described in Section 10.12.
        '''
        session = req.context['session']
        if session.is_guest():
            raise falcon.HTTPUnauthorized('Please Login')

        # Validate scope
        scope = req.params.get('scope')
        if not scope:
            raise falcon.HTTPBadRequest(
                title='Bad request',
                description='Missing scope in request',
            )
        scope = scope.split()
        invalid = [s for s in scope if not ObjectId.is_valid(s)]
        if invalid:
            raise falcon.HTTPBadRequest(
                title='Bad request',
                description='Scope ID invalid, should be 24-digit hex: %s' % invalid,
            )
        auth_grps = Meta.auth_group_model.find({
            'id': {'$in': [ObjectId(s) for s in scope]}
        })
        diff = set(scope) - set([str(a.id) for a in auth_grps])
        if diff:
            raise falcon.HTTPBadRequest(
                title='Bad request',
                description='Scope ID invalid: %s' % list(diff),
            )
        try:
            grant = Meta.oauth_server.validate_consent_request(req)
        except Exception as e:
            raise falcon.HTTPBadRequest(
                title='Bad request',
                description=str(e),
            )
        ret = grant.request.args
        ret['client_name'] = grant.request.client.client_name
        ret['scope'] = [ str(s.id) for s in auth_grps ]
        ret['scope_info'] = [ {
            'name': s.name,
            'description': s.description,
            'id': str(s.id),
        } for s in auth_grps ]
        resp.status = falcon.HTTP_200
        resp.media = ret

    @falcon.before(LoginRequired())
    def on_post(self, req, resp):
        session = req.context['session']
        user = session.user
        self.oauth_response(
            resp, *Meta.oauth_server.create_authorization_response(req, grant_user=user)
        )

    def on_issueToken(self, req, resp):
        '''
        grant_type
             REQUIRED.  Value MUST be set to "authorization_code".

        code
             REQUIRED.  The authorization code received from the
             authorization server.

        redirect_uri
             REQUIRED, if the "redirect_uri" parameter was included in the
             authorization request as described in Section 4.1.1, and their
             values MUST be identical.

        client_id
             REQUIRED, if the client is not authenticating with the
             authorization server as described in Section 3.2.1.
        '''
        self.oauth_response(resp, *Meta.oauth_server.create_token_response(req))