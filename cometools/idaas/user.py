class AppUser(object):
    def __init__(self, username, email, phone_number):
        self.username = username
        self.email = email or "UNKNOWN"
        self.phone_number = phone_number or "UNKNOWN"

    @classmethod
    def from_jwt(cls, decoded_id_token):
        return cls(
            decoded_id_token['sub'],
            decoded_id_token['email'],
            decoded_id_token['mobile'],
        )
    
    @classmethod
    def from_oauth(cls, user_info):
        return cls(
            user_info['username'],
            user_info['email'],
            user_info['phone_number']
        )