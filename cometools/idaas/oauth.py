from authlib.integrations.requests_client.oauth2_session import OAuth2Session
import requests
import urllib.parse
from cometools.idaas.user import AppUser


class OAuthClient(object):
    AUTH_ENDPOINT = "oauth/authorize"
    TOKEN_ENDPOINT = "oauth/token"
    USER_INFO_API_ENDPOINT = "api/bff/v1.2/oauth2/userinfo"

    def __init__(self, idaas_server_url, client_id, client_secret, redirect_uri, scope='read'):
        self.idaas_auth_point = urllib.parse.urljoin(
            idaas_server_url, self.AUTH_ENDPOINT)
        self.idaas_token_point = urllib.parse.urljoin(
            idaas_server_url, self.TOKEN_ENDPOINT)
        self.idaas_user_point = urllib.parse.urljoin(
            idaas_server_url, self.USER_INFO_API_ENDPOINT)
        self.client = OAuth2Session(
            client_id, client_secret, scope=scope, redirect_uri=redirect_uri)
        self.auth_client = None

    def get_authorize_url(self):
        return self.client.create_authorization_url(self.idaas_auth_point, prompt="login")[0]

    def get_user(self):
        if not self.auth_client:
            return
        response = self.auth_client.get(self.idaas_user_point)
        return AppUser.from_oauth(response.json()['data'])

    def auth(self, code):
        token = self.client.fetch_token(
            self.idaas_token_point, grant_type="authorization_code", code=code)
        self.auth_client = OAuth2Session(
            self.client.client_id, self.client.client_secret, token=token)

