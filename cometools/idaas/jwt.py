from authlib.jose import jwt
from cometools.idaas.user import AppUser

class IDTokenResolver(object):
    def __init__(self, public_key):
        self.public_key = public_key

    def resolve(self, id_token):
        return AppUser.from_jwt(jwt.decode(id_token, self.public_key))
