class Meta:
    __slots__ = [
        # OAuth Server Side
        'oauth_server',
        'oauth_client_model',
        'oauth_token_model',
        'oauth_authcode_model',
        'oauth_grants',
        # OAuth Client Side
        'oauth_client',
        'oauth_registry_model',
        'ouath_user_token_model',
        # Permission system
        'permission_model',
        'auth_group_model',
        'user_model',
        'session_redis_model',
        'settings',
    ]
    @classmethod
    def config(cls, **kwargs):
        for k, v in kwargs.items():
            if k not in cls.__slots__:
                raise Exception('Unexpected meta config: %s' % k)
            setattr(cls, k, v)
        cls.init_oauth()
        cls.validate()

    @classmethod
    def init_oauth(cls):
        from cometools.utils.oauth.server import OAuth2AuthorizationServer
        from cometools.utils.oauth_client.oauth_registry import OAuth
        if cls.oauth_client_model and cls.oauth_token_model and cls.oauth_grants:
            cls.oauth_server = OAuth2AuthorizationServer(
                cls.oauth_client_model,
                cls.oauth_token_model,
                config=cls.settings.get('oauth2_server'),
            )
            for grant in cls.oauth_grants:
                cls.oauth_server.register_grant(grant)
        if cls.oauth_registry_model and cls.ouath_user_token_model:
            cls.oauth_client = OAuth()
    
    @classmethod
    def validate(cls):
        for k in cls.__slots__:
            if type(getattr(cls, k)).__name__ == 'member_descriptor':
                raise Exception('Require "%s" in Meta object' % k)
