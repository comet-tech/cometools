from cometools.meta import Meta

class SessionMiddleware(object):
    def process_resource(self, req, resp, resource, params):
        ''' Save app options.env to context
        '''
        req.context['env'] = resource.application.options['env']

    def process_request(self, req, resp):
        ''' Assign a server-side session for each access
        '''
        cookies = req.cookies
        session_id = cookies.get('session_id')
        if session_id is None:
            session_id = Meta.session_redis_model.create_new_session()
            resp.set_cookie('session_id', session_id, path='/', http_only=False)
        session = Meta.session_redis_model(session_id)
        if not session.is_valid():
            session_id = Meta.session_redis_model.create_new_session()
            resp.set_cookie('session_id', session_id, path='/', http_only=False)
            session = Meta.session_redis_model(session_id)
        req.context['session'] = session
        auth_str = req.headers.get('AUTHORIZATION')
        if auth_str:
            token_type, access_token = auth_str.split()
            token = Meta.oauth_token_model.find_one({
                'token_type': token_type,
                'access_token': access_token,
            })
            req.context['token'] = token