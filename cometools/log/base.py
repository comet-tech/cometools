import logging


class BaseFormatter(logging.Formatter):
    def __init__(self, prefix=None):
        fmt = '[%(levelname)1.1s %(asctime)s %(filename)s:%(lineno)d] %(message)s'
        if prefix:
            fmt = prefix+fmt
        super(BaseFormatter, self).__init__(
            fmt=fmt,
            datefmt='%y%m%d %H:%M:%S',
            style='%')


class BaseLogConfigurator(object):
    @classmethod
    def config_root(cls):
        root_logger = logging.getLogger()
        root_logger.setLevel(logging.DEBUG)
        formatter = BaseFormatter()
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        root_logger.addHandler(handler)
