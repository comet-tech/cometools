import logstash
import logging
from cometools.log.base import BaseLogConfigurator, BaseFormatter


class LogstashStreamHandler(logging.StreamHandler):
    def setFormatter(self):
        formatter = BaseFormatter(prefix="[LOGSTASH]")
        super(LogstashStreamHandler, self).setFormatter(formatter)
        return self


class LogstashConfigurator(BaseLogConfigurator):
    LOGGER_PREFIX = "logstash"

    @classmethod
    def __get_logstash_root_logger(cls):
        return logging.getLogger(cls.LOGGER_PREFIX)

    @classmethod
    def config_logstash_tcp(cls, host, port):
        cls.config_root()
        logger = cls.__get_logstash_root_logger()
        logger.setLevel(logging.INFO)
        logger.propagate = False
        logger.addHandler(logstash.TCPLogstashHandler(host, port, version=1))
        logger.addHandler(LogstashStreamHandler().setFormatter())

    @classmethod
    def config_logstash_udp(cls, host, port):
        cls.config_root()
        logger = cls.__get_logstash_root_logger()
        logger.setLevel(logging.INFO)
        logger.propagate = False
        logger.addHandler(logstash.LogstashHandler(host, port, version=1))
        logger.addHandler(LogstashStreamHandler().setFormatter())
