from types import ModuleType

def collectpkgs(current_module):
    pkgs = []
    
    for obj in current_module.__dict__.values():
        if isinstance(obj, ModuleType) and \
                current_module.__name__ in obj.__name__ and \
                obj.__package__ != current_module.__name__:
            pkgs.append(obj)
            pkgs.extend(
                collectpkgs(obj)
            )
    return pkgs 