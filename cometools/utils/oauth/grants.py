from authlib.oauth2.rfc6749 import grants
from authlib.common.security import generate_token
from bson import ObjectId
from cometools.meta import Meta

class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    def create_authorization_code(self, client, grant_user, request):
        code = generate_token(48)
        item = Meta.oauth_authcode_model(
            code=code,
            client_id=client.client_id,
            redirect_uri=request.redirect_uri,
            response_type=request.response_type,
            scope=request.scope.split(),
            user=str(grant_user.id),
        )
        item.save()
        return code

    def parse_authorization_code(self, code, client):
        item = Meta.oauth_authcode_model.find_one({
            'code': code,
            'client_id': ObjectId(client.client_id),
        })
        if item is not None and not item.is_expired():
            return item

    def delete_authorization_code(self, authorization_code):
        authorization_code.delete()

    def authenticate_user(self, authorization_code):
        return authorization_code.user

class ClientCredentialsGrant(grants.ClientCredentialsGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = [
        'client_secret_post'
    ]

class RefreshTokenGrant(grants.RefreshTokenGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = [
        'client_secret_post'
    ]
    INCLUDE_NEW_REFRESH_TOKEN = True

    def authenticate_refresh_token(self, refresh_token):
        item = Meta.oauth_token_model.find_one({
            'refresh_token': refresh_token
        })
        if item is None:
            return None
        elif item.is_refresh_token_active():
            return item

    def authenticate_user(self, credential):
        return credential.user

    def revoke_old_credential(self, credential):
        credential.set(revoked=True)
    
    def issue_token(self, client, user, credential):
        expires_in = credential.get_expires_in()
        scope = self.request.scope
        if not scope:
            scope = credential.get_scope()
        token = self.generate_token(
            client, self.GRANT_TYPE,
            user=user,
            expires_in=expires_in,
            scope=scope.split(),
            include_refresh_token=self.INCLUDE_NEW_REFRESH_TOKEN,
        )
        return token