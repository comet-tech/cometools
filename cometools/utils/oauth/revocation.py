from authlib.oauth2.rfc7009 import RevocationEndpoint as _RevocationEndpoint


class RevocationEndpoint(_RevocationEndpoint):
    def query_token(self, token, token_type_hint, client):
        """Query requested token from database."""
        token_model = self.server.token_model
        if token_type_hint == 'access_token':
            rv = _query_access_token(token_model, token)
        elif token_type_hint == 'refresh_token':
            rv = _query_refresh_token(token_model, token)
        else:
            rv = _query_access_token(token_model, token)
            if not rv:
                rv = _query_refresh_token(token_model, token)

        client_id = client.get_client_id()
        if rv and rv.client_id == client_id:
            return rv
        return None

    def revoke_token(self, token):
        """Mark the give token as revoked."""
        token.revoked = True
        token.save()


def _query_access_token(token_model, token):
    return token_model.find_one({
        'access_token': token_model
    })


def _query_refresh_token(token_model, token):
    return token_model.find_one({
        'refresh_token': token
    })
