import logging
import os
from contextlib import contextmanager
from functools import wraps

import inflection
import sqlalchemy.sql.functions as func
# import ujson
from sqlalchemy import Column
from sqlalchemy import create_engine
from sqlalchemy import DateTime
from sqlalchemy import DECIMAL
from sqlalchemy import event
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Table
from sqlalchemy.exc import DisconnectionError
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import mapper
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy.sql.sqltypes import JSON, TIMESTAMP
import sqlalchemy.sql.sqltypes as sa
from sqlalchemy.sql.expression import cast
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.attributes import flag_modified

from sqlalchemy import or_
import sqlalchemy.engine.result
import sqlalchemy.orm.query
import functools
from contextvars import ContextVar
import attr
from datetime import datetime
from bson import ObjectId

logger = logging.getLogger('mysql')

class MysqlDict(dict):
    def __init__(self, inst, field_name, *args, **kwargs):
        self.inst = inst
        self.field_name = field_name
        return super(self, MysqlDict).__init__(
            *args, **kwargs
        )

    def __setitem__(self, key, item):
        flag_modified(self.inst, self.field_name)
        self.__dict__[key] = item

    def update(self, *args, **kwargs):
        flag_modified(self.inst, self.field_name)
        return self.__dict__.update(*args, **kwargs)
    
    def pop(self, *args):
        flag_modified(self.inst, self.field_name)
        return self.__dict__.pop(*args)

class MYSQL_JSON_WRAPPER:
    def __init__(self, field_name):
        self.field_name = '_%s' % field_name

    def __get__(self, instance, owner):
        # Call as class method
        if instance is None and owner:
            return getattr(owner, self.field_name)
        # Call as instance method
        if getattr(instance, self.field_name) is None:
            return None
        return MysqlDict(
            self.field_name,
            instance,
            getattr(instance, self.field_name),
        )

    def __set__(self, instance, value):
        if value is None:
            setattr(instance, self.field_name, None)
        flag_modified(instance, self.field_name)

    def __delete__(self, instance):
        setattr(instance, self.field_name, None)
        flag_modified(self, self.field_name)

# Exception
class MysqlModelError(Exception):
    UNKNOWN = 0
    NO_CONNECTION = 100
    DB_NOT_EXISTS = 101

    def __init__(self, message=None, error_code=0):
        message = "[code %s] %s" % (error_code, message)
        super(MysqlModelError, self).__init__(message)
        self.error_code = error_code

    def __repr__(self):
        return "code %s: %s" % self.error_code, self.message

Base = declarative_base()

class MysqlConnection(object):
    def __init__(self):
        self.Engine = None
        self.params = None

    @property
    def engine(self):
        if self.Engine is not None:
            return self.Engine
        else:
            raise MysqlModelError(error_code=MysqlModelError.NO_CONNECTION)

    def session(self, **kwargs):
        if self.Engine is None:
            raise MysqlModelError(error_code=MysqlModelError.NO_CONNECTION)
        else:
            _session = sessionmaker(
                bind=self.Engine.execution_options(**kwargs)
            )()
            return _session

    def connect(self, host, port, db, username, password, debug=False, **kwargs):
        self.params = {
            "host": host,
            "port": port,
            "db": db,
            "username": username,
            "password": password,
            "debug": debug,
        }
        uri = (
            "mysql+pymysql://%(username)s:%(password)s@%(host)s"
            ":%(port)s/%(db)s" % self.params
        )
        self.Engine = create_engine(
            uri, connect_args={"connect_timeout": 30}, echo=debug, pool_recycle=6400
        )
        logger.info("Connecting MYSQL database: %(host)s:%(port)s/%(db)s" % self.params)

        @event.listens_for(self.Engine, "connect")
        def connect(dbapi_connection, connection_record):
            connection_record.info['pid'] = os.getpid()

        @event.listens_for(self.Engine, "checkout")
        def checkout(dbapi_connection, connection_record, connection_proxy):
            pid = os.getpid()
            if connection_record.info['pid'] != pid:
                connection_record.connection = connection_proxy.connection = None
                raise DisconnectionError(
                    "Connection record belongs to pid %s, "
                    "attempting to check out in pid %s" %
                    (connection_record.info['pid'], pid)
                )

        @event.listens_for(self.Engine, "before_cursor_execute")
        def before_cursor_execute(
            conn, cursor, statement, parameters, context, executemany
        ):
            # TODO - logging call stack
            return

        @event.listens_for(self.Engine, "after_cursor_execute")
        def after_cursor_execute(
            conn, cursor, statement, parameters, context, executemany
        ):
            # TODO
            return

class Mysql:
    __connections = {}

    @classmethod
    def get_connection(cls, name='default'):
        if name not in cls.__connections:
            cls.__connections[name] = MysqlConnection()
        return cls.__connections[name]

SESSION_CONTEXT = ContextVar('mysql_session')

def current_session():
    try:
        return SESSION_CONTEXT.get()
    except LookupError:
        raise LookupError('Current session not found in this context.')

@contextmanager
def session_scope(conn='default', inherit_session=True, **kwargs):
    """Provide a transactional scope around a series of operations."""
    def patched_commit():
        # Clean customer rollback task when commit session
        session.rollback_tasks = []
        origin_commit()

    def patched_rollback():
        # Execute customer rollback task when rollback session
        for d in session.rollback_tasks:
            f = d.pop("handler")
            f(**d)
        origin_rollback()

    if inherit_session:
        try:
            session = current_session()
            yield session
            return
        except LookupError:
            pass
    conn = Mysql.get_connection(conn)
    # Initial new session and set scope
    session = conn.session(**kwargs)
    setattr(session, "rollback_tasks", [])
    origin_commit = session.commit
    origin_rollback = session.rollback
    session.commit = patched_commit
    session.rollback = patched_rollback
    try:
        token = SESSION_CONTEXT.set(session)
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        SESSION_CONTEXT.reset(token)
        session.close()

def unique_id():
    return str(ObjectId())

@as_declarative()
class MysqlBaseTable:
    id = Column(String(24), default=unique_id, primary_key=True)
    _meta = Column('meta', JSON, default=lambda : {})
    meta = MYSQL_JSON_WRAPPER('meta')
    created_ts = Column(DateTime, default=func.now(), index=True, nullable=False)
    modified_ts = Column(DateTime, default=func.now(), index=True, nullable=False, onupdate=func.now())

    __conn__ = 'default' # master connection of this table

    # pylint: disable=E0213
    @declared_attr
    def __tablename__(cls):
        return inflection.underscore(cls.__name__)

    @classmethod
    def tablename(cls):
        return cls.__tablename__

    @classmethod
    def conn(cls):
        return Mysql.get_connection(cls.__conn__)

    @classmethod
    def by_id(cls, id, columns=None, for_update=False):
        session = current_session()
        if columns:
            if isinstance(columns, (tuple, list)):
                query = session.query(*columns)
            else:
                raise MysqlModelError("Invalid columns format!")
        else:
            query = session.query(cls)

        if for_update:
            query = query.with_for_update()
        return query.filter_by(id=id).first()

    @classmethod
    def fields(cls):
        ret = []
        for k, v in cls.__dict__.items():
            if isinstance(v, InstrumentedAttribute):
                ret.append(k)
        return ret

    def save(self):
        if self.id is None:
            self.id=unique_id()
        current_session().add(self)
        return self

    def to_dict(self, datetime_format='%Y%m%d %H:%M:%S'):
        d = {k: v for k, v in self.__dict__.items() if not k.startswith("_")}
        for k, v in d.items():
            if isinstance(v, datetime) and datetime_format:
                d[k] = v.strftime(datetime_format)
        return d

class MysqlPartitionMixin:
    pass # TODO