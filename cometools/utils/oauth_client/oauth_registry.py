from .remote_app import RemoteApp
from cometools.meta import Meta
from authlib.integrations._client import OAuth as _OAuth

__all__ = ['OAuth']

class OAuth(_OAuth):
    remote_app_class = RemoteApp

    def fetch_token(self, name, request=None):
        if request:
            session = request.context['session']
            if not session.user:
                raise Exception('User not logged in')
            user_id = session.user.id
        else:
            user_id = None
        token = Meta.ouath_user_token_model.find_one({
            'name': name,
            'user_id': user_id,
        })
        return token.to_dict_default() if token else None
    
    def update_token(self, name, token, refresh_token=None, access_token=None):
        if refresh_token:
            item = Meta.ouath_user_token_model.find_one({
                'name': name,
                'refresh_token': refresh_token,
            })
        elif access_token:
            item = Meta.ouath_user_token_model.find_one({
                'name': name,
                'access_token': access_token,
            })
        else:
            return

        # update old token
        item.update_one({
            '$set': {
                'access_token': token['access_token'],
                'refresh_token': token.get('refresh_token'),
                'expires_at': token.get('expires_at'),
                'expires_in': token.get('expires_in'),
            }
        })

    def __init__(self, *args, **kwargs):
        kwargs['fetch_token'] = kwargs.get('fetch_token', self.fetch_token)
        kwargs['update_token'] = kwargs.get('update_token', self.update_token)
        super(OAuth, self).__init__(*args, **kwargs)

    def load_config(self, name, params):
        return _get_conf(name)

def _get_conf(name):
    obj = Meta.oauth_registry_model.find_one({
        'name': name
    })
    if obj:
        return obj.to_dict_default()
    else:
        return None

