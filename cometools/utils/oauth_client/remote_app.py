from authlib.integrations._client import OAuth as _OAuth
from authlib.integrations._client  import UserInfoMixin
from authlib.integrations._client  import RemoteApp as _RemoteApp
from authlib.integrations.requests_client import OAuth2Session as _OAuth2Session
from authlib.integrations.requests_client.oauth2_session import OAuth2Auth as _OAuth2Auth
from cometools.meta import Meta
from authlib.common.urls import (
    urlparse,
    add_params_to_uri,
    add_params_to_qs,
)
from authlib.integrations._client import (
    MissingRequestTokenError,
    MissingTokenError,
    InvalidTokenError,
)


__all__ = ['token_update', 'RemoteApp']


# token_update = Signal(providing_args=['name', 'token', 'refresh_token', 'access_token'])

# class OAuth2Auth(_OAuth2Auth):
#     def ensure_active_token(self, **kwargs):
#         if self.client and self.token.is_expired():
#             refresh_token = self.token.get('refresh_token')
#             client = self.client
#             url = client.metadata.get('token_endpoint')
#             if refresh_token and url:
#                 client.refresh_token(url, refresh_token=refresh_token, **kwargs)
#             elif client.metadata.get('grant_type') == 'client_credentials':
#                 access_token = self.token['access_token']
#                 token = client.fetch_token(
#                     url, grant_type='client_credentials', **kwargs)
#                 if client.update_token:
#                     client.update_token(token, access_token=access_token)
#             else:
#                 raise InvalidTokenError()

# class OAuth2Session(_OAuth2Session):
#     def __init__(self, *args, **kwargs):
#         self.token_auth_class = OAuth2Auth
#         super(OAuth2Session, self).__init__(*args, **kwargs)

class RemoteApp(_RemoteApp, UserInfoMixin):
    def __init__(self, *args, **kwargs):
        super(RemoteApp, self).__init__(*args, **kwargs)
        # self.oauth2_client_cls = OAuth2Session

    def _get_session_data(self, request, key):
        sess_key = '_{}_authlib_{}_'.format(self.name, key)
        session = request.context['session']
        ret = session.container().get(sess_key)
        if ret:
            ret = ret.decode('ascii')
            del session.container()[sess_key]
        return ret

    def _set_session_data(self, request, key, value):
        sess_key = '_{}_authlib_{}_'.format(self.name, key)
        session = request.context['session']
        session.container()[sess_key]=value

    def _generate_access_token_params(self, request):

        if self.request_token_url:
            return request.params

        if request.method == 'GET':
            params = {
                'code': request.get_param('code'),
                'state': request.get_param('state'),
            }
        else:
            params = {
                'code': request.media.get('code'),
                'state': request.media.get('state'),
            }
        return params

    def authorize_redirect(self, req, resp, redirect_uri=None, **kwargs):
        session = req.context['session']
        rv = self.create_authorization_url(redirect_uri, **kwargs)
        self.save_authorize_data(request, redirect_uri=redirect_uri, **rv)
        raise falcon.HTTPFound(rv['url'])

    def get_token_by_user(self, user):
        token = Meta.ouath_user_token_model.find_one({
            'name': self.name,
            'user_id': user.id,
        })
        if token:
            return token.to_dict_default()
        else:
            return None

    def authorize_access_token(self, request, **kwargs):
        """Fetch access token in one step.

        :param request: HTTP request instance from Django view.
        :return: A token dict.
        """
        params = self.retrieve_access_token_params(request)
        params.update(kwargs)
        return self.fetch_access_token(**params)
    
    def fetch_client_credentials_token(self):
        metadata = self._load_server_metadata()
        token_endpoint = self.access_token_url
        if not token_endpoint and not self.request_token_url:
            token_endpoint = metadata.get('token_endpoint')
        
        with self._get_oauth_client(**metadata) as client:
            kwargs = {'grant_type': 'client_credentials'}
            token = client.fetch_token(token_endpoint, **kwargs)
            # cfg = oauth.load_config(app_name, {})
        # token = oauth_session.authorize_access_token(req)

        # self.handle_token(req, resp, token, cfg)
        # TODO - Save Token if safe_for_user is set
        Meta.ouath_user_token_model.update({
            'name': self.name,
            'user_id': None,
        }, {
            '$set': token
        }, upsert=True)
        return token

    def parse_id_token(self, request, token, claims_options=None):
        return self._parse_id_token(request, token, claims_options)
    
    def request(self, method, url, token=None, **kwargs):
        if self.api_base_url and not url.startswith(('https://', 'http://')):
            url = urlparse.urljoin(self.api_base_url, url)

        kwds = {}
        if token is None:
            kwds = {
                'grant_type': 'client_credentials'
            }
        with self._get_oauth_client(**kwds) as session:
            request = kwargs.pop('request', None)
            if kwargs.get('withhold_token'):
                return session.request(method, url, **kwargs)
            if token is None and self._fetch_token:
                token = self._fetch_token(request)
            if token is None and request is None:
                # When request is none, using client credential
                token = self.fetch_client_credentials_token()

            session.token = token
            return session.request(method, url, **kwargs)