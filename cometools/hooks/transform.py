from datetime import datetime
from bson import ObjectId

def sanitize_mongo_query(req, resp, resource, params):
    def __convert_ts_to_id(x):
        if isinstance(x, int):
            try:
                dt = datetime.fromtimestamp(x / 1000.0)
                new_x = ObjectId.from_datetime(dt)
            except Exception:
                new_x = x
        elif isinstance(x, dict):
            new_x = {}
            for k, v in x.items():
                new_x[k] = __convert_ts_to_id(v)
        else:
            new_x = x
        return new_x

    def __convert_created_ts_dict(x):
        if isinstance(x, dict):
            new_x = {}
            for k, v in x.items():
                if k == 'created_ts':
                    kval = x.pop(k)
                    new_x['_id'] = __convert_ts_to_id(kval)
                else:
                    new_x[k] = __convert_created_ts_dict(v)
        elif isinstance(x, list):
            new_x = []
            for l in x:
                new_x.append(__convert_created_ts_dict(l))
        else:
            new_x = x
        return new_x
    if 'q' not in req.context:
        req.context['q'] = dict(req.media)
    data = req.context['q']
    if data.get('fields'):
        data['fields'] = [
            f if f != 'created_ts' else 'id' for f in data['fields']]
        # force add id
        if 'id' not in data['fields']:
            data['fields'].append('id')
        # rename to fit mongo schema
        data['projection'] = data.pop('fields')
    if data.get('sort'):
        data['sort'] = [
            t if t[0] != 'created_ts' else ['_id', t[1]]
                for t in data['sort']
        ]
    data['query'] = __convert_created_ts_dict(data['query'])
    req.context['q'] = data

class ListIndexTransformer:
    def __init__(self, targets, field='_index', val_key='_data'):
        if not isinstance(targets, list):
            targets = [targets]
        self.targets = []
        for t in targets:
            self.targets.append(t.split('.'))
        self.field = field
    
    def __call__(self, req, resp, resource):
        def __update(d, target, ret):
            if len(target) == 1:
                d[target[0]] = ret
            else:
                d[target[0]] = __update(d[target[0]], target[1:], ret)
            return d

        for target in self.targets:
            t = resp.media
            for f in target:
                t = t[f]
            if not isinstance(t, list):
                return
            ret = []
            for idx, r in enumerate(t):
                if not isinstance(r, dict):
                    ret.append({
                        self.field: idx,
                        self.val_key: r,
                    })
                else:
                    ret.append({
                        '_index': idx,
                    })
                    ret[idx].update(r)
            resp.media = __update(resp.media, target, ret)
class FieldsFilter:
    def __init__(self, target=None):
        self.target = target
    
    def __call__(self, req, resp, resource):
        data = resp.media.pop(self.target) if self.target else resp.media
        if not isinstance(data, list):
            return
        ret = []
        if req.method in ['POST', 'PUT', 'PATCH']:
            fields = req.media.get('fields')
        elif req.method in ['GET']:
            fields = req.params.get('fields')
            if isinstance(fields, str):
                fields = fields.split(',')
        if not fields:
            return
        for idx, r in enumerate(data):
            if not isinstance(r, dict):
                continue
            new_r = dict(r)
            for k in r:
                if k not in fields:
                    new_r.pop(k)
            ret.append(new_r)
        ret = {
            self.target: ret
        }
        ret.update(resp.media)
        resp.media = ret