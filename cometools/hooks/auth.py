import falcon
# from demo.model.mongo.rbac import Permission
from urllib.parse import urlparse
from cometools.meta import Meta
from cometools.mixin.oauth import TokenMixin
from datetime import datetime

class LoginRequired:
    def __init__(self, title='Please login.'):
        self.title = title

    def __call__(self, req, resp, resource, params):
        session = req.context['session']
        if req.context['env']['config'] == 'stage' and \
                req.headers.get('MAGIC-CODE') == 'WHOSYOURDADDY':
            return
        if session.is_guest():
            raise falcon.HTTPUnauthorized(self.title)

class OAuthRequired:
    def __init__(self, title='No permission to proceed', method=None):
        self.title = title
        self.method = method
    
    def __call__(self, req, resp, resource, params):
        token = req.context.get('token')
        if not token:
            raise falcon.HTTPForbidden('OAuth token is missing or invalid.')
        token_expire_ts = datetime.fromtimestamp(token.get_expires_at())
        if datetime.utcnow() > token_expire_ts:
            raise falcon.HTTPForbidden('Token expired.')
        method = req.method if self.method is None else self.method
        permissions = token.get_permissions()
        if not Meta.permission_model.check_permissions(
                req.relative_uri,
                Meta.permission_model.Action.name_to_num(method),
                permissions):
            raise falcon.HTTPForbidden('OAuth token does not has permission to proceed')
        # Checking user permission
        user_permissions = token.get_user().get_permissions()
        if not Meta.permission_model.check_permissions(
                req.relative_uri,
                Meta.permission_model.Action.name_to_num(method),
                user_permissions):
            raise falcon.HTTPForbidden('User does not has permission to proceed')
class PermissionRequired:
    def __init__(self, title='No permission to proceed', method=None):
        self.title = title
        self.method = method

    def __call__(self, req, resp, resource, params):
        LoginRequired()(req, resp, resource, params)
        # Backdoor for stage development
        if req.context['env']['config'] == 'stage' and \
                req.headers.get('MAGIC-CODE') == 'WHOSYOURDADDY':
            return
        session = req.context['session']
        user = req.context['session'].user
        permissions = req.context['session'].permissions
        method = req.method if self.method is None else self.method
        if not Meta.permission_model.check_permissions(
                req.relative_uri,
                Meta.permission_model.Action.name_to_num(method),
                permissions):
            raise falcon.HTTPForbidden(self.title)