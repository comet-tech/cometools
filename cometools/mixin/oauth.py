from authlib.oauth2.rfc6749 import AuthorizationCodeMixin as _AuthorizationCodeMixin
from authlib.oauth2.rfc6749 import ClientMixin as _ClientMixin
from authlib.oauth2.rfc6749 import TokenMixin as _TokenMixin
from datetime import datetime
from cometools.meta import Meta
import iu_mongo.fields as f
from bson import ObjectId

class TokenMixin(_TokenMixin):
    meta = {
        'indexes': [
            {'keys': 'access_token:1'},
            {'keys': 'client_id:1'},
            {'keys': 'user:1'},
        ],
    }

    ALLOWED_TOKEN_TYPE = [
        'Bearer'
    ]

    user = f.ObjectIdField()
    client_id = f.ObjectIdField()
    token_type = f.StringField(choices=ALLOWED_TOKEN_TYPE)
    access_token = f.StringField()
    refresh_token = f.StringField()
    scope = f.ListField(f.StringField())
    revoked = f.BooleanField(default=False)
    expires_in = f.IntField(default=0)

    @property
    def issued_at(self):
        return int(self.id.generation_time.strftime('%s'))

    def get_client(self):
        return Meta.oauth_client_model.by_id(self.client_id)

    def get_client_id(self):
        return str(self.client_id)

    def get_scope(self):
        return self.scope

    def get_expires_in(self):
        return self.expires_in

    def get_expires_at(self):
        return self.issued_at + self.expires_in

    def get_roles(self):
        return Meta.auth_group_model.by_ids([
            ObjectId(s) for s in self.scope
        ])
    
    def get_permissions(self):
        return Meta.permission_model.get_permissions(self.get_roles(), [])

    def get_client_user(self):
        return Meta.user_model.by_id(
            Meta.oauth_client_model.by_id(self.client_id).user
        )

    def get_user(self):
        return Meta.user_model.by_id(self.user)
    
    def is_refresh_token_active(self):
        return not self.revoked

class ClientMixin(_ClientMixin):
    meta = {
        'indexes': [
            {'keys': 'access_token:1'},
            {'keys': 'client_name:1'},
            {'keys': 'user:1'},
        ],
    }

    ALLOWED_RESPONSE_TYPE = [
        'code',
        'token',
    ]
    ALLOWED_GRANT_TYPE = [
        'authorization_code',
        'refresh_token',
        'client_credentials',
    ]
    ALLOWED_CLIENT_AUTH_METHOD = [
        'client_secret_post'
    ]

    user = f.ObjectIdField()
    client_secret = f.StringField()
    client_name = f.StringField()
    redirect_uris = f.ListField(f.StringField())
    default_redirect_uri = f.StringField()
    scope = f.ListField(f.StringField())
    response_types = f.ListField(f.StringField(choices=ALLOWED_RESPONSE_TYPE))
    grant_type = f.ListField(f.StringField(choices=ALLOWED_GRANT_TYPE))
    token_endpoint_auth_method = f.StringField(choices=ALLOWED_CLIENT_AUTH_METHOD)

    @property
    def client_id(self):
        return str(self.id)

    def get_client_id(self):
        return self.client_id

    def get_default_redirect_uri(self):
        return self.default_redirect_uri

    def get_allowed_scope(self, scopes):
        if not scopes:
            return []
        if not isinstance(scopes, list):
            scopes = scopes.split()
        allowed = self.scope
        return [s for s in scopes if s in allowed]

    def check_redirect_uri(self, redirect_uri):
        if redirect_uri == self.default_redirect_uri:
            return True
        return redirect_uri in self.redirect_uris

    def has_client_secret(self):
        return bool(self.client_secret)

    def check_client_secret(self, client_secret):
        return self.client_secret == client_secret

    def check_token_endpoint_auth_method(self, method):
        return self.token_endpoint_auth_method == method

    def check_response_type(self, response_type):
        allowed = self.response_types
        return response_type in allowed

    def check_grant_type(self, grant_type):
        allowed = self.grant_type
        return grant_type in allowed


class AuthorizationCodeMixin(_AuthorizationCodeMixin):
    meta = {
        'indexes': [
            {'keys': 'code:1'},
            {'keys': 'client_id:1'},
            {'keys': 'user:1'},
        ],
    }
    user = f.ObjectIdField()
    client_id = f.ObjectIdField()
    code = f.StringField()
    redirect_uri = f.StringField()
    response_type = f.StringField()
    scope = f.ListField(f.StringField())

    @property
    def auth_time(self):
        return int(self.id.generation_time.strftime('%s'))

    def is_expired(self):
        return self.auth_time + 300 < int(datetime.utcnow().strftime('%s'))

    def get_redirect_uri(self):
        return self.redirect_uri

    def get_scope(self):
        return self.scope

    def get_auth_time(self):
        return self.auth_time

class ClientRegistryMixin:
    meta = {
        'indexes': [
            {'keys': 'name:1', 'unique': True},
        ],
    }
    name = f.StringField()
    client_id = f.StringField()
    client_secret = f.StringField()
    access_token_url = f.StringField()
    authorize_url = f.StringField()
    api_base_url = f.StringField()
    client_kwargs = f.DictField()

class UserTokenMixin:
    meta = {
        'indexes': [
            {'keys': 'user_id:1,name:1', 'unique': True},
        ],
    }
    user_id = f.ObjectIdField()
    name = f.StringField()
    token_type = f.StringField()
    access_token = f.StringField()
    refresh_token = f.StringField()
    expires_in = f.IntField()
    expires_at = f.IntField()
    scope = f.DictField()