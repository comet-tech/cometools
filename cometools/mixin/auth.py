from cometools.utils.py_enum import PyEnumMixin
from bson import ObjectId
import re
from urllib.parse import urlparse
from cometools.meta import Meta
import iu_mongo.fields as f

class PermissionMixin:
    class Action(PyEnumMixin):
        GET = 0
        POST = 1
        PUT = 2
        PATCH = 3
        DELETE = 4

    allow = f.BooleanField()
    resource = f.StringField() # Regex for web endpoint
    actions = f.ListField(f.IntField(choices=Action.get_ids()))

    def __repr__(self):
        return '%s.%s %s' % (self.resource, self.actions, self.allow)

    @classmethod
    def santize_url_resource(cls, resource, permission):
        if resource[0] != '/':
            return resource
        path = urlparse(resource).path
        r_items = path.rstrip('/').split('/')
        p_items = permission.resource.rstrip('/').split('/')
        r_items[-1] = r_items[-1].split(':')[0]
        return '/'.join(r_items)+'/'

    @classmethod
    def check_permissions(cls, resource, action, permissions, default_allow=False):
        for p in permissions:
            r = cls.santize_url_resource(resource, p)
            pattern = '^{url_pattern}'.format(
                url_pattern = p.resource
            )
            if re.match(pattern, r) and (
                    action in p.actions or p.actions == []):
                return p.allow
        else:
            return default_allow

    @classmethod
    def get_permissions(cls, roles, permissions):
        plist = []
        resource_set = set()
        for p in permissions:
            if p.resource not in resource_set:
                plist.append(p)
                resource_set.add(p.resource)
        for r in roles:
            rp = r.get_permissions()
            for p in rp:
                if p.resource not in resource_set:
                    plist.append(p)
                    resource_set.add(p.resource)
        return plist

class AuthGroupMixin:
    meta = {
        'indexes': [
            {'keys': 'name:1', 'unique': True},
            {'keys': 'tags:1'},
            {'keys': 'permissions.resource:1,permissions.actions:1,permissions.allow:1'},
        ],
    }
    name = f.StringField()
    description = f.StringField()
    parents = f.ListField(f.ObjectIdField())
    permissions = f.ListField(f.EmbeddedDocumentField('Permission'))
    tags = f.ListField(f.StringField())

    def __repr__(self):
        return '%s:%s' % (self.id, self.name)

    @property
    def created_ts(self):
        return self.id.generation_time.strftime('%Y-%m-%d %H:%M:%S')

    def get_permissions(self):
        parents = Meta.auth_group_model.by_ids(self.parents)
        return Meta.permission_model.get_permissions(parents, self.permissions)

class UserMixin:
    meta = {
        'indexes': [
            {'keys': 'username:1', 'unique': True},
        ],
    }
    username = f.StringField()
    password = f.StringField()
    permissions = f.ListField(f.EmbeddedDocumentField('Permission'),  default=[])
    roles = f.ListField(f.ObjectIdField(),  default=[])

    @property
    def created_ts(self):
        return self.id.generation_time

    def get_user_id(self):
        return self.id

    def get_permissions(self):
        return Meta.permission_model.get_permissions(self.get_roles(), self.permissions)

    def get_roles(self):
        return Meta.auth_group_model.by_ids(self.roles)