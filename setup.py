import os
import sys
from setuptools import find_packages, setup

DESCRIPTION = (
    'Common Tools for Comet-tech'
)

try:
    with open('README.md') as fin:
        LONG_DESCRIPTION = fin.read()
except Exception:
    LONG_DESCRIPTION = None


def get_version(version_tuple):
    """Return the version tuple as a string, e.g. for (0, 10, 7),
    return '0.10.7'.
    """
    return '.'.join(map(str, version_tuple))


# Dirty hack to get version number from monogengine/__init__.py - we can't
# import it as it depends on PyMongo and PyMongo isn't installed until this
# file is read
init = os.path.join(os.path.dirname(__file__), 'cometools', '__init__.py')
version_line = list(filter(lambda l: l.startswith('VERSION'), open(init)))[0]

VERSION = get_version(eval(version_line.split('=')[-1]))

extra_opts = {
    'packages': find_packages(exclude=['tests', 'tests.*']),
}

REQUIRES = [
    'iu-mongo>=1.0.0',
    'gunicorn>=19.9.0',
    'passlib>=1.7',
    'celery>=4.2.0',
    'inflection>=0.3.1',
    'gevent>=1.4.0',
    'redis>=3.2.1',
    'requests>=2.21.0',
    'docopt>=0.6.2',
    'ipython>=7.4.0',
    'pymongo>=3.7',
    'Authlib>=0.13',
    'jsonschema>=3.0.1',
    'pyyaml>=5.1',
    'falcon>=2.0.0',
    'walrus>=0.7.1',
    'python-logstash>=0.4.6',
    'coloredlogs>=14.0',
    'colorlog>=4.0.0',
    'attrs>=19.3.0',
    "python-logstash==0.4.6"
]

setup(
    name='cometools',
    version=VERSION,
    author='Arthur Mao',
    author_email='arthur@comet-tech.com.cn',
    maintainer="Arthur Mao",
    maintainer_email="arthur@comet-tech.com.cn",
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    platforms=['any'],
    license='MIT',
    install_requires=REQUIRES,
    python_requires='>=3.7',
    **extra_opts
)
