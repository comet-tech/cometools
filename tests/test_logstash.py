import logging
import unittest
from cometools.log.logstash_conf import LogstashConfigurator


class LogstashTest(unittest.TestCase):
    def test_logstash(self):
        LogstashConfigurator.config_logstash_tcp("localhost", 5044)
        logger = logging.getLogger("logstash.database.table")
        # this log will not be sent to logstash
        logger.debug('debug test')
        logger.info('info test')
        logger.warning('warn test', extra={'extra_data': "xxx"})
        logger.error('error test')
        logging.getLogger('test').info('normal log')